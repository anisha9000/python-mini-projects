from os import listdir, rename ,makedirs, remove
from os.path import isfile, join , exists
from re import split, sub
from time import sleep
import sys
import string

path = "C:/Users/anpoddar/Downloads/songs/all songs/"
#path = "C:/Users/anpoddar/Downloads/test/"
org_stdout = sys.stdout
f = file(path+"logs.txt",'w')
sys.stdout = f
#get files in a directory
filenames = listdir(path)
for i in filenames :
    updated_file_name = list()
    new_list = split('-|_| |\. |\[[^]]*',i)
    updated_file_name = new_list[:]
    print i
    for index,j in enumerate(new_list) : 
        new_index = updated_file_name.index(j)
        if j in (None, '', '(', ')', '[', ']') :
            if j in updated_file_name:
                updated_file_name.remove(j)
                continue
        if j[0] in ( ']',')') :           
            updated_file_name[new_index] = updated_file_name[new_index].replace(j[0],"",1)
            continue
        if j.strip().isdigit() :
            updated_file_name.remove(j)
            continue
        if (string.find(j,'.com') or string.find(j,'.COM')) != -1 :
            updated_file_name[new_index] = sub('.*\.com|.*\.COM','',updated_file_name[new_index],0)
            continue
        if (string.find(j,'.pk') or string.find(j,'.PK')) != -1 :
            updated_file_name[new_index] = sub('.*\.pk|.*\.PK','',updated_file_name[new_index],0)
            #updated_file_name.remove(j)
            continue
        if (string.find(j,'.in') or string.find(j,'.IN')) != -1 :
            updated_file_name[new_index] = sub('.*\.in|.*\.IN','',updated_file_name[new_index],0)
            #updated_file_name.remove(j)
            continue
        for char_index,c in enumerate(j) :
            if c.isdigit(): 
                updated_file_name[new_index] = updated_file_name[new_index].replace(c,"",1)
            else :
                break           
        updated_file_name[new_index] = updated_file_name[new_index].replace(" ", "")
        updated_file_name[new_index] = updated_file_name[new_index].capitalize()
    #print updated_file_name    
    new_file =  ' '.join(updated_file_name)
    new_file = new_file.strip()
    print new_file
    oldfile = path+i
    newfile = path+new_file
    try :
        rename(oldfile, newfile)
        #sleep(2)
    except WindowsError as detail:
        '''
        if not exists(path+'rename_error') : 
            makedirs(path+'rename_error')
        sleep(2)
        print "file exists:" , detail , i
        new_path = path+'rename_error/'+i
        rename(oldfile, new_path)
        '''
        print "file exists:",detail